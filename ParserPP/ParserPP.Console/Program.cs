﻿using edu.stanford.nlp.ling;
using edu.stanford.nlp.parser.lexparser;
using edu.stanford.nlp.process;
using edu.stanford.nlp.trees;
using java.io;
using ParserPP.Console.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Console = System.Console;

namespace ParserPP.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine(" ");
            System.Console.Write("PRZYKŁAD DLA GRAMATYKI Z KSIĄŻKI");
            System.Console.WriteLine(" ");

            Gramatic gramaticPP = new Gramatic();
            gramaticPP.AddGramatic("S -> NP VP");
            gramaticPP.AddGramatic("NP -> NP PP");
            gramaticPP.AddGramatic("NP -> Noun");
            gramaticPP.AddGramatic("VP -> Verb NP");
            gramaticPP.AddGramatic("VP -> VP PP");
            gramaticPP.AddGramatic("PP -> Prep NP");
            gramaticPP.AddGramatic("Noun -> (John|Mary|Denver)");
            gramaticPP.AddGramatic("Verb -> (called)");
            gramaticPP.AddGramatic("Prep -> (from)");

            EarleyParser statesPPGramatic = new EarleyParser(gramaticPP, "John called Mary from Denver");

            int index = 0;
            foreach (var state in statesPPGramatic.StateSets)
            {
                System.Console.Write("==== S" + index.ToString() + " ====");
                System.Console.WriteLine(" ");
                foreach (var gramaticItem in state.GramaticItems)
                {
                    foreach (var gramaticModel in gramaticItem.Value)
                    {
                        System.Console.Write(gramaticModel.gramaticStart + " -> ");
                        foreach (var item in gramaticModel.dividedModel)
                        {
                            System.Console.Write(item.item + " ");

                        }
                        System.Console.WriteLine(" ");
                    }

                }
                System.Console.WriteLine(" ");
                index++;
            }


            System.Console.WriteLine(" ");
            System.Console.Write("PRZYKŁAD DLA GRAMATYKI RÓWNANIA");
            System.Console.WriteLine(" ");

            Gramatic gramatic = new Gramatic();
            gramatic.AddGramatic("Sum -> Sum [+-] Product");
            gramatic.AddGramatic("Sum -> Product");
            gramatic.AddGramatic("Product -> Product [*/] Factor");
            gramatic.AddGramatic("Product -> Factor");
            gramatic.AddGramatic("Factor -> '(' Sum ')'");
            gramatic.AddGramatic("Factor -> Number");
            gramatic.AddGramatic("Number -> [0-9]+");
            gramatic.AddGramatic("Number -> [0-9]+ Number");


            //EarleyParser states = new EarleyParser(gramatic, "1 + ( 2 * 3 - 4 )");
            EarleyParser states = new EarleyParser(gramatic, "1 + ( 2 * 3 )");

            index = 0;
            foreach (var state in states.StateSets)
            {
                System.Console.Write("==== S" + index.ToString() + " ====");
                System.Console.WriteLine(" ");
                foreach (var gramaticItem in state.GramaticItems)
                {
                    foreach (var gramaticModel in gramaticItem.Value)
                    {
                        System.Console.Write(gramaticModel.gramaticStart + " -> ");
                        foreach (var item in gramaticModel.dividedModel)
                        {
                            System.Console.Write(item.item + " ");

                        }
                        System.Console.WriteLine(" ");
                    }
                  
                }
                System.Console.WriteLine(" ");
                index++;
            }



            Gramatic gramaticEngLanguage = new Gramatic();
            gramaticEngLanguage.AddGramatic("S -> NP VP");
            gramaticEngLanguage.AddGramatic("NP -> Det N");
            gramaticEngLanguage.AddGramatic("NP -> Det Adj N");
            gramaticEngLanguage.AddGramatic("VP -> V");
            gramaticEngLanguage.AddGramatic("VP -> V NP");
            gramaticEngLanguage.AddGramatic("Det -> (The|a|an)");
            gramaticEngLanguage.AddGramatic("Adj -> (black|white)");
            gramaticEngLanguage.AddGramatic("N -> (cat|mouse)");
            gramaticEngLanguage.AddGramatic("V -> (ate)");


            //EarleyParser states = new EarleyParser(gramatic, "1 + ( 2 * 3 - 4 )");
            EarleyParser earleyParserForEngGramatic = new EarleyParser(gramaticEngLanguage, "The black cat ate a white mouse");

            System.Console.WriteLine(" ");
            System.Console.Write("PRZYKŁAD DLA GRAMATYKI JĘZYKA ANGIELSKIEGO");
            System.Console.WriteLine(" ");


            index = 0;
            foreach (var state in earleyParserForEngGramatic.StateSets)
            {
                System.Console.Write("==== S" + index.ToString() + " ====");
                System.Console.WriteLine(" ");
                foreach (var gramaticItem in state.GramaticItems)
                {
                    foreach (var gramaticModel in gramaticItem.Value)
                    {
                        System.Console.Write(gramaticModel.gramaticStart + " -> ");
                        foreach (var item in gramaticModel.dividedModel)
                        {
                            System.Console.Write(item.item + " ");
                            if(item.item== "Zakończono przetwarzanie")
                            {
                                System.Console.WriteLine("Otrzymano na wyjściu:");
                                System.Console.WriteLine("- Gramatyke od której zaczęto przetwarzanie:");
                                System.Console.WriteLine("- Stan przetwarzania jest na koncu gramatyki przetwarzanej");
                                System.Console.WriteLine("- ");
                            }

                        }
                        System.Console.WriteLine(" ");
                    }

                }
                System.Console.WriteLine(" ");
                index++;
            }

           
            System.Console.ReadKey();
        }
    }
}
