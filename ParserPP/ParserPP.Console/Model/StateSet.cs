﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserPP.Console.Model
{
    public class StateSet
    {
        public string Item { get; set; }
        public Dictionary<string, List<GramaticModel>> GramaticItems { get; set; }
        public string GramaticStart { get; set; }
    }
}
