﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserPP.Console.Model
{
    public class Gramatic
    {
        public Dictionary<string, List<GramaticModel>> context;
        public string GrammaStart
        {
            get;
            private set;
        }
        public Gramatic()
        {
            context = new Dictionary<string, List<GramaticModel>>();
        }

        public void AddGramatic(string gramatic)
        {
            var item = new GramaticModel(gramatic);

            if (!context.GetEnumerator().MoveNext())
            {
                GrammaStart = item.gramaticStart;
            }

            if (context.ContainsKey(item.gramaticStart))
            {
                context[item.gramaticStart].Add(item);
            }
            else
            {
                context.Add(item.gramaticStart, new List<GramaticModel>() { item });
            }
        }
    }
}
