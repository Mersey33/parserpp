﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ParserPP.Console.Model
{
    public struct GramaticModel
    {


        // a support class to divide a grammar in a string and interprete it.
        // a constructor throw exception when we didnt have a -> in string
        // a constructor get a Gramatic start property and rest of the gramatic

        public GramaticModel(string gramaticModel)
        {
            if (!gramaticModel.Contains("->"))
            {
                throw new InvalidOperationException();
            }
            this.OryginalModel = gramaticModel;

            this.dividedModel = new List<GramaticItem>();
            string[] splitGramatic = gramaticModel.Split(' ');

            this.gramaticStart = splitGramatic[0];
            var dividedgramaticalModel = splitGramatic.Skip(2).ToList();
            this.gramaticStateNumber = 0;

            foreach (var gramaticItem in dividedgramaticalModel)
            {
                var item = CheckItem(gramaticItem);
                dividedModel.Add(item);
            }
        }
        public List<GramaticItem> dividedModel;
        public string gramaticStart;
        public int gramaticStateNumber;
        public string OryginalModel { get; set; }

        public int IncrementGramaticStateNumber()
        {
            return gramaticStateNumber++;
        }
        public int IncrementGramaticStateNumber(int index)
        {
            gramaticStateNumber = index;
            return gramaticStateNumber;
        }

        public void BeginGramma()
        {
            gramaticStateNumber = 0;
            for (int i = 0; i < dividedModel.Count; i++)
            {
                if(dividedModel.ElementAt(i).itemType==GramaticItemType.Dot)
                {
                    dividedModel.RemoveAt(i);
                    break;
                }
            }
            dividedModel.Insert(0, new GramaticItem()
            {
                item = "*",
                itemType = GramaticItemType.Dot
            });
        }
        public bool MoveDot()
        {
            for (int i = 0; i < dividedModel.Count; i++)
            {
                if (dividedModel.ElementAt(i).itemType == GramaticItemType.Dot)
                {
                    if (dividedModel.Count == i + 1)
                    {
                        return false;
                    }
                    else
                    {
                        var temp = dividedModel.ElementAt(i);
                        dividedModel.RemoveAt(i);
                        i++;
                        dividedModel.Insert(i, temp);
                        return true;
                    }

                }
            }
            return false;
        }

        public GramaticItem NextStepGramma()
        {
            for (int i = 0; i < dividedModel.Count; i++)
            {
                if (dividedModel.ElementAt(i).itemType == GramaticItemType.Dot)
                {
                    if (i + 1 >= dividedModel.Count)
                    {
                        return new GramaticItem();
                    }
                    else
                    {                         
                        return dividedModel.ElementAt(i + 1);
                    }
                }
            }
            return new GramaticItem();
        }
        public GramaticItem GetNextGramatic()
        {
            int index = 0;
            foreach (var model in dividedModel)
            {
                index++;
                if (model.itemType == GramaticItemType.Dot)
                {
                    break;
                }

            }
            //int dotPosition = dividedModel.First(n=>n.itemType==GramaticItemType.Dot)
            if (index + 1 <= dividedModel.Count)
            {
                return dividedModel.ElementAt(index++);
            }
            else
            {
                return new GramaticItem();
            }
        }

        public GramaticItem GetFirstItem()
        {
            return dividedModel.ElementAt(0);
        }

        private GramaticItem CheckItem(string item)
        {
            if (item.Contains('\''))
            {
                if (item.ElementAt(item.Length - 1) == '\'')
                {
                    Regex regex = new Regex("'");
                    return new GramaticItem()
                    {
                        item = regex.Replace(item, ""),
                        itemType = GramaticItemType.Word
                    };
                }
            }
            if (item.Contains('[') || (item.Contains('(') && item.Contains(')')))
            {
                return new GramaticItem()
                {
                    item = item,
                    itemType = GramaticItemType.Enumeration
                };


            }

            return new GramaticItem()
            {
                item = item,
                itemType = GramaticItemType.Gramatic
            };

        }

        public GramaticModel Copy()
        {
            var copyList = new List<GramaticItem>();
            foreach (var dividedElement in dividedModel)
            {
                copyList.Add(new GramaticItem()
                {
                    item = dividedElement.item,
                    itemType = dividedElement.itemType
                });
            }
            return new GramaticModel(this.OryginalModel)
            {
                OryginalModel = OryginalModel,
                dividedModel = copyList,
                gramaticStart = this.gramaticStart,
                gramaticStateNumber = this.gramaticStateNumber

            };

        }

    }
}
