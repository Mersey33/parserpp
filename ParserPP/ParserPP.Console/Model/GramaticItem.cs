﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserPP.Console.Model
{
    public struct GramaticItem
    {

        public string item { get; set; }
        public GramaticItemType itemType { get; set; }
    }
}
