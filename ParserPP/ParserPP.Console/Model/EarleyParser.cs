﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ParserPP.Console.Model
{
    public class EarleyParser
    {
        private Gramatic gramaticModel;
        string[] sentenceModel;
        public EarleyParser(Gramatic gramatic, string sentence)
        {
            gramaticModel = gramatic;
            sentenceModel = sentence.Split(' ');
            //initialize a states in earley parser
            //we didnt parse anything so we initialize a formally array - for me list because i can easier opearte on this

            StateSets = new List<StateSet>();

            //at the begining we have to set a start position and current position to zero.
            //StartPosition = 0;
            //CurrentPosition = 0;

            //we putting dot at the begingn of the parse array [ in mind ofc]
            // s -> * s ...          

            Initialize();

        }

        public List<StateSet> StateSets { get; set; }
        public bool isCorrect { get; set; }

        // in earley parser we have two types of loops inside in gramatic.
        // outter loop -> we loop over a main array (stateSets)
        // inner loop -> we loop over a single set



        private void Initialize()
        {
            StateSet stateSet = new StateSet();
            //we setting the gramma for a state S(0) 
            stateSet.GramaticStart = gramaticModel.GrammaStart;
            //we eliminate duplicates and setting gramma at the beginning 
            //we eliminate duplicates because we have to avoid a infinity loops
            stateSet.GramaticItems = new Dictionary<string, List<GramaticModel>>(gramaticModel.context);

            //In gramaticToCheck we store a items that we have to check that could be matched with grammatic rules
            List<GramaticModel> gramaticToCheck = new List<GramaticModel>();
            string wordToCheck = sentenceModel[0];

            foreach (KeyValuePair<string, List<GramaticModel>> entry in stateSet.GramaticItems)
            {
                foreach (var grammaRule in entry.Value)
                {
                    //we setting dot at the beggining
                    grammaRule.BeginGramma();

                }
                var oryginalGramatic = MatchWords(entry.Value, wordToCheck);
                var checkedGrammatic = new List<GramaticModel>();
                foreach (var item in oryginalGramatic)
                {
                    checkedGrammatic.Add(item.Copy());
                }

                if (checkedGrammatic != null)
                {
                    gramaticToCheck = gramaticToCheck.Concat(checkedGrammatic).ToList();
                }
            }
            StateSets.Add(stateSet);
            isCorrect = NextState(1, new List<GramaticModel>(gramaticToCheck));

        }

        public bool NextState(int index, List<GramaticModel> stepGramma)
        {

            var currentStepGramma = new List<GramaticModel>();
            foreach (var model in stepGramma)
            {
                //przesuniecie kropki o jedno miejsce w prawo
                var copy = model.Copy();
                copy.MoveDot();

                currentStepGramma.Add(copy);
            }
            //if (index >= sentenceModel.Count())
            //{
            //    var z = currentStepGramma.First().GetNextGramatic().item;


            //}
            string word = string.Empty;
            if (index >= sentenceModel.Count())
            {
                word = "brak";
            }
            else {
                word = sentenceModel.ElementAt(index);
            }
            bool nextState = false;
            int modelIndex = 0;


            do
            {
                if (modelIndex >= currentStepGramma.Count)
                {
                    StateSets.Add(new StateSet()
                    {
                        GramaticItems = ListToDictionary(currentStepGramma),
                        GramaticStart = currentStepGramma.ElementAt(0).gramaticStart,
                        Item = "Zakończono przetwarzanie"
                    });
                    return true;
                }
                var model = currentStepGramma.ElementAt(modelIndex);

                //zwracany jest element ZA KROPKA
                var currentModelItem = model.NextStepGramma();
                //istnieje wyraz na prawo od kropki
                if (currentModelItem.item != null)
                {
                    // dwa przypadki
                    //mamy symbol terminalny
                    //mamy symbol nieterminalny - slowo - enumeracja

                    switch (currentModelItem.itemType)
                    {
                        case GramaticItemType.Gramatic:
                            string key = currentModelItem.item;
                            var listToConcat = new List<GramaticModel>();
                            var copyList = gramaticModel.context[key];
                            foreach (var item in copyList)
                            {
                                //kopiujemy element
                                var copyItem = item.Copy();
                                copyItem.BeginGramma();
                                //ustawiamy stan w ktorym zostala utworzona
                                copyItem.IncrementGramaticStateNumber(index);
                                listToConcat.Add(copyItem);
                            }
                            //konkatenujemy listy elementow
                            currentStepGramma = currentStepGramma.Concat(listToConcat).ToList();
                            break;

                        case GramaticItemType.Word:
                            if (word == currentModelItem.item)
                            {
                                List<GramaticModel> modelCopy = new List<GramaticModel>();
                                modelCopy.Add(model.Copy());

                                StateSets.Add(new StateSet()
                                {
                                    Item = word,
                                    GramaticItems = ListToDictionary(currentStepGramma),
                                    GramaticStart = stepGramma.First().gramaticStart
                                });
                                NextState(index + 1, modelCopy);
                                nextState = true;
                            }
                            break;
                        case GramaticItemType.Enumeration:
                            Regex regex = new Regex(currentModelItem.item);
                            if (regex.IsMatch(word))
                            {
                                List<GramaticModel> modelCopy = new List<GramaticModel>();
                                modelCopy.Add(model.Copy());

                                StateSets.Add(new StateSet()
                                {
                                    Item = word,
                                    GramaticItems = ListToDictionary(currentStepGramma),
                                    GramaticStart = stepGramma.First().gramaticStart
                                });
                                NextState(index + 1, modelCopy);
                                //zakonczenie dzialania
                                nextState = true;
                            }
                            break;
                    }
                }
                else
                {
                    Dictionary<string, List<GramaticModel>> previousStateGramaticModel = new Dictionary<string, List<GramaticModel>>();

                    int stateIndex = model.gramaticStateNumber;

                    if (stateIndex == index)
                    {
                        previousStateGramaticModel = ListToDictionary(currentStepGramma);
                    }
                    else
                    {
                        previousStateGramaticModel = StateSets.ElementAt(stateIndex).GramaticItems;
                    }

                    //wybralismy juz stan, z ktorego bedziemy pobierac dane
                    //nalezy poszukac czy istnieje jakas regula, w ktorej na prawo od kropki znajduje sie dany symbol


                    foreach (KeyValuePair<string, List<GramaticModel>> entry in previousStateGramaticModel)
                    {
                        foreach (var value in entry.Value)
                        {
                            int dotIndex = 0;
                            foreach (var item in value.dividedModel)
                            {
                                if (item.itemType == GramaticItemType.Dot)
                                {
                                    break;
                                }
                                else
                                {
                                    dotIndex++;
                                }

                            }
                            if (dotIndex + 1 < value.dividedModel.Count)
                            {
                                if (value.dividedModel.ElementAt(dotIndex + 1).item == model.gramaticStart)
                                {
                                    var copyValue = value.Copy();
                                    copyValue.MoveDot();
                                    currentStepGramma.Add(copyValue);
                                }

                            }
                        }
                    }

                }
                modelIndex++;

                RemoveCopies(currentStepGramma);
            } while (!nextState);

            return true;

        }


        private void RemoveCopies(List<GramaticModel> gramaticItems)
        {
            bool findCopy;
            for (int i = 0; i < gramaticItems.Count; i++)
            {
                var currentGramatic = gramaticItems.ElementAt(i);
                for (int j = i + 1; j < gramaticItems.Count; j++)
                {
                    var currentComparer = gramaticItems.ElementAt(j);
                    findCopy = true;
                    if (currentGramatic.dividedModel.Count != currentComparer.dividedModel.Count)
                    {
                        continue;
                    }
                    for (int index = 0; index < currentComparer.dividedModel.Count; index++)
                    {
                        if (currentComparer.dividedModel.ElementAt(index).item != currentGramatic.dividedModel.ElementAt(index).item)
                        {
                            findCopy = false;
                            continue;
                        }

                    }
                    if (findCopy)
                    {
                        //znaleziono kopie
                        gramaticItems.RemoveAt(j);
                        j--;
                    }

                }
            }
        }
        private List<GramaticModel> MatchWords(List<GramaticModel> model, string word)
        {
            List<GramaticModel> gramaticToCheck = new List<GramaticModel>();
            foreach (var item in model)
            {
                for (int i = 0; i < item.dividedModel.Count; i++)
                {
                    if (item.dividedModel.ElementAt(i).itemType == GramaticItemType.Dot)
                    {
                        if (i + 1 <= item.dividedModel.Count && item.dividedModel.ElementAt(i + 1).itemType != GramaticItemType.Gramatic)
                        {
                            if (MatchWord(item.dividedModel.ElementAt(i + 1), word))
                            {
                                gramaticToCheck.Add(item);
                            }

                        }
                    }
                }
            }

            return gramaticToCheck;
        }
        private bool MatchWord(GramaticItem item, string word)
        {
            if (item.itemType == GramaticItemType.Word)
            {
                if (item.item == word)
                {
                    return true;
                }
                else {
                    return false;
                }
            }
            else
            {
                Regex regex = new Regex(item.item);
                return regex.IsMatch(word);
            }
        }
        private Dictionary<string, List<GramaticModel>> ListToDictionary(List<GramaticModel> input)
        {
            Dictionary<string, List<GramaticModel>> context = new Dictionary<string, List<GramaticModel>>();
            foreach (var item in input)
            {
                if (context.ContainsKey(item.gramaticStart))
                {
                    context[item.gramaticStart].Add(item);
                }
                else
                {
                    context.Add(item.gramaticStart, new List<GramaticModel>() { item });
                }
            }
            return context;

        }

    }
}
